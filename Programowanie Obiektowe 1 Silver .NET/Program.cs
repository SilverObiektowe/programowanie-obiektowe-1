﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programowanie_Obiektowe_1_Silver.NET
{
    //Napisz klasę Car
    //Dodaje do niej pola kolor i typNadwozia.
    //Kolor i typNadwozia mają typ string. 
    //Dodaj do klasy Car metode o nazwie Jedz, która nic nie zwraca i nie ma parametrów
    //Dodaj metodę Tankuj, która posiada parametr typu int iloscPaliwa, 
    //Hamuj, która zwraca typ bool
    //Dodaj konstruktor do klasy, który w parametrach przyjmuje kolor i 
    //typ nadwozia i zapisuje je do wcześniej stworzonych pól
    //Następnie stwórz 3 obiekty tej klasy.

    //Gdy skończysz wszystko powyżej sprubuj napisać za pomocą 
    //Windows Forms prosty symulator wyścigów samochodów.
    //Wykorzystaj do wyświetlenia kontrolkę PictureBox.
    //Jeśli trzeba dodaj do klasy odpowiednie pola i metody.

    public class Program
    {
        public static void Main(string[] args)
        {
            ForTests.Car1 = new Car("", "");
            ForTests.Car2 = new Car("", "");
            ForTests.Car3 = new Car("", "");
        }
    }

    #region Dla testów
    public static class ForTests
    {
        private static object car1;
        public static object Car1
        {
            set { car1 = value; }
            get { return car1; }
        }

        private static object car2;
        public static object Car2 { set { car2 = value; } get { return car2; } }

        private static object car3;
        public static object Car3 { set { car3 = value; } get { return car3; } }
    }
    #endregion
}
